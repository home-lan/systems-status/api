import psutil
import time
import socket

from flask import Flask
from flask import jsonify

app = Flask(__name__)
app.debug = True # Uncomment to debug

# this is a very rough first attempt at retrieving this data
# original source: https://learn.pimoroni.com/tutorial/networked-pi/raspberry-pi-system-stats-python
# plan to clean up and make more robust at some point

@app.route('/stats')
def stats():
    stats = generate_stats()
    return jsonify(stats)

def generate_stats():
    return {
        'cpuPercentageUsed': str(psutil.cpu_percent()) + '%',
        'memory': memory(),
        'disk': disk(),
        'temperature': temp(),
        'uptime': uptime(),
        'hostname': hostname()
    }

def hostname():
     return socket.gethostbyaddr(socket.gethostname())[0]

def uptime():
     seconds_uptime = time.time() - psutil.boot_time()
     days_uptime = seconds_uptime / 60 / 60 / 24
     days_uptime = str(days_uptime) + ' days'
     return days_uptime

def temp():
    temps = psutil.sensors_temperatures(fahrenheit=True)
    temp = str(temps['cpu-thermal'][0][1]) + ' F'
    return temp

def memory():
    memory = psutil.virtual_memory()
    # Divide from Bytes -> KB -> MB
    available = round(memory.available/1024.0/1024.0,1)
    total = round(memory.total/1024.0/1024.0,1)
    return str(available) + 'MB free / ' + str(total) + 'MB total ( ' + str(memory.percent) + '% )'

def disk():
    disk = psutil.disk_usage('/')
    # Divide from Bytes -> KB -> MB -> GB
    free = round(disk.free/1024.0/1024.0/1024.0,1)
    total = round(disk.total/1024.0/1024.0/1024.0,1)
    return str(free) + 'GB free / ' + str(total) + 'GB total ( ' + str(disk.percent) + '% )'

if __name__ == '__main__':
    app.run(host='0.0.0.0')
